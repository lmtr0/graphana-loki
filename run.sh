podman stop --all && podman rm --all

podman run --network host --name loki -d -v $(pwd):/mnt/config grafana/loki -config.file=/mnt/config/loki-config.yaml
podman run --network host --name graphana -d  grafana/grafana